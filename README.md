# Taro-app

#### 介绍
Taro App

[git 地址](https://gitee.com/study_1/Taro-app.git)

#### 软件架构
Taro App 统一多端运行

#### git操作说明
- 需要先查看状态 git status
- 然后在添加要提交的文件到git本地仓库 git add -A
- 添加注释提交 git commit -m '提交内容说明'
- 推送到远程git仓库 git push
- 查看推送日志 git log
- 重置 git reset head HEAD

#### 安装教程
- 安装 @tarojs/cli环境 
  - npm install -g @tarojs/cli
    - 注意查看node版本 更新node , npm版本
    - node -v
    - npm -v, 升级npm命令：npm install -g npm
    - 查看镜像 npm get registry
    - 更换镜像地址 npm set registry https://registry.npm.taobao.org/

使用淘宝镜像 cnpm
- npm install -g cnpm --registry=https://registry.npm.taobao.org    

[Taro UI框架](https://taro-ui.aotu.io/#/docs/introduction)
- npm install taro-ui

- 构建Taro模板
  - taro init taroApp
- 启动
  - dev环境启动
    - npm run dev:h5 WEB
    - npm run dev:weapp 微信小程序
    - npm run dev:swan 百度小程序
    - npm run dev:alipay 支付宝小程序

 - taro对应小程序的生命周期
   - componentWillMount
     - 在微信小程序中这一生命周期方法对应页面的onLoad或入口文件app中的onLaunch
   - componentDidMount
     - 在微信小程序中这一生命周期方法对应页面的onReady或入口文件app中的onLaunch，在 componentWillMount后执行
   - componentDidShow
     - 在微信小程序中这一生命周期方法对应 onShow
   - componentDidHide
     - 在微信小程序中这一生命周期方法对应 onHide
   - componentDidCatchError
     - 错误监听函数，在微信小程序中这一生命周期方法对应 onError
   - componentDidNotFound
     - 页面不存在监听函数，在微信小程序中这一生命周期方法对应 onPageNotFound
   - shouldComponentUpdate
     - 页面是否需要更新
   - componentWillUpdate
     - 页面即将更新
   - componentDidUpdate
     - 页面更新完毕
   - componentWillUnmount
     - 页面退出，在微信小程序中这一生命周期方法对应 onUnload  

#### 使用说明

1. git clone
2. npm install
3. npm run dev:h5
---
> 注意更新node , npm版本
- 查看版本 node -v 

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 参考

完美解决：https://blog.csdn.net/yi_master/article/details/86009294

- 多端运行 https://segmentfault.com/a/1190000016766906?utm_source=tag-newest