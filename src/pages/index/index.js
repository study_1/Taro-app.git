import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import Head from '../components/head/head'
import Food from '../components/food/food'
import Bottom from '../components/bottom/bottom'
import './index.css'
import img0 from '../static/img/r.jpg'
import img1 from '../static/img/scan.png'
import img2 from '../static/img/find.png'

export default class Index extends Component {

  config = {
    navigationBarTitleText: '首页'
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  skipToDetail(){
    /*  */
  }
  render () {
    return (
      <View className='index'>
        <Swiper indicatorDots autoplay>
        {[img0,img1,img2].map(img=>(<SwiperItem key={img}><Image src={img} /></SwiperItem>))}
        </Swiper>
        <Head />
        <Food />
        <Bottom />
      </View>
    )
  }
}