import Taro, { Component } from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import './top.css'

class Top extends Component{
    render(){
        return(<View className='top'>
            <View className='left'>
                <Image className='left_img' src={require('../../static/img/left.png')}></Image>
            </View>
            <View className='right'>
                <Image className='right_img' src={require('../../static/img/find.png')}></Image>
                <Image className='right_img' src={require('../../static/img/star.png')}></Image>
                <Image className='right_img' src={require('../../static/img/left.png')}></Image>
            </View>
        </View>)
    }
}
export default Top;
