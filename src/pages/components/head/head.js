/* eslint-disable no-unused-vars */
import Taro, { Component } from '@tarojs/taro';
import { View, Text, Image } from '@tarojs/components';
import Top from './top';
import Activity from './activity';
import './head.css';

class Head extends Component{
    constructor(){
        super(...arguments)
        this.state={
            "store":{
                title:"美食圈",
                notice:"欢迎光临，很高兴为您服务~",
                tags:["味道赞","主食丰盛","分量足"]
            }
        }
    }
    render(){
        let {store} = this.state;
        return(<View className='head'>
            <Top />
            <Image className='back' src={require('../../static/img/Background.png')}></Image>
            <View className='store'>
                <Image className='store_img' src={require('../../static/img/r.jpg')}></Image>
                <View className='store_txt'>
                <Text>{store.title}</Text>
                <Text>{store.notice}</Text>
                <View>
                    {store.tags.map((item,index)=><Text className='tags_txt' key='index'>{item}</Text>)}
                </View>
            </View>
            </View>
            <Activity />
        </View>)
    }
}
export default Head;
