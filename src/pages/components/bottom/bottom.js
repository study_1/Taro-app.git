/* eslint-disable react/no-unused-state */
/* eslint-disable no-unused-vars */
import Taro, { Component } from '@tarojs/taro';
import { View, Text, Image } from '@tarojs/components';
import './bottom.css';

class Bottom extends Component{
    constructor(){
        super(...arguments)
        this.state={
           Num:1,
           sendPrice:3,
           supportTake:false,
           sendPriceMore:20
        }
    }
    render(){
        let {Num,sendPrice,supportTake,sendPriceMore} = this.state;
        return(<View className='bottom'>
        <View className='bottom_body'>
        {Num?<Text className='num'>{Num}</Text>:null}
            <Image className='store_img' src={require('../../static/img/cart.png')}></Image>
            <View>
                <Text className='info'>{sendPrice?"另需配送费￥ |"+sendPrice:""}</Text>
                <Text>{supportTake?"支持自取":"不支持自取"}</Text>
            </View>
            <View className='submit'><Text>{sendPriceMore?"￥"+sendPriceMore+"起送":""}</Text></View>
        </View>

        </View>)
    }
}
export default Bottom;
