import Taro, { Component } from '@tarojs/taro';
import { View } from '@tarojs/components';
import { AtTabs, AtTabsPane } from 'taro-ui';

class Food extends Component{
    constructor(){
        super(...arguments);
        this.state={
            current:1,
            tabList:[{title:"点餐"},{title:"评价"},{title:"商家"}]
        }
    }
    changeTab(value){
        this.setState({current:value})
    }
    render(){
        //解构赋值 
        let {current, tabList} = this.state;
        return(<View>
           <AtTabs  tabDirection='horizontal' current={current} onClick={this.changeTab.bind(this)} tabList={tabList}>
                <AtTabsPane></AtTabsPane>
           </AtTabs>
        </View>)
    }
}
export default Food;
